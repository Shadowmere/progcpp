#include <iostream>
#include <stdlib.h>
#include <memory>
#include <algorithm>

template <typename T>
class Wektor final
{
  public:
	using value_type = T;

	Wektor()
		: Wektor(0)
	{}

	Wektor(const std::size_t size)
		: n(size)
		, tab(new value_type[size])
	{}

	Wektor(const Wektor& w)
		: Wektor(w.n)
	{
		std::copy(w.tab.get(), std::next(w.tab.get(), w.n), tab.get());
	}

	Wektor(Wektor&& w)
		: Wektor(w.n)
	{
		w.n = 0;
		tab.reset(w.tab.release()); // take over ownership -> extreamly fast, for example for copy elision
	}

	Wektor& operator=(const Wektor& a)
	{
		tab.reset(new value_type[a.n]);
		this->n = a.n;
		std::copy(a.tab.get(), std::next(a.tab.get(), a.n), tab.get());
		return *this;
	}

	Wektor& operator=(Wektor&& a)
	{
		tab.reset(new value_type[a.n]);
		this->n = a.n;
		a.n = 0;
		this->tab.reset(a.tab.release()); // take over ownership -> extreamly fast, for example for copy elision
		return *this;
	}

	~Wektor() = default;

	void print() const
	{
		print(std::cout);
	}

	void print(std::ostream& out) const
	{
		for (int i = 0; i < n; i++)
			out << tab[i] << " ";
		out << std::endl;
	}

	void set(const value_type& x)
	{
		if (tab)
		{
			for (int i = 0; i < n; i++)
				tab[i] = x;
		}
	}

	const value_type& operator[](const std::size_t el) const
	{
		return tab[el];
	}

	Wektor<value_type> operator+(const Wektor<value_type>& a);

	template<typename V>
	friend std::ostream& operator<<(std::ostream& out, const Wektor<V>& w);

  private:
	std::size_t n;
	std::unique_ptr<value_type[]> tab;
};

template<typename T>
std::ostream& operator<<(std::ostream& out, const Wektor<T>& w)
{
	w.print(out);
}

template <typename value_type>
Wektor<value_type> Wektor<value_type>::operator+(const Wektor<value_type>& a)
{
	Wektor<value_type> wynik{n + a.n};

	std::copy(tab.get(), tab.get() + n, wynik.tab.get());
	std::copy(a.tab.get(), a.tab.get() + a.n, wynik.tab.get() + n);

	return wynik;
}

using DoubleWektor = Wektor<double>; 

int main()
{
	DoubleWektor w1(5);
	w1.set(3.14);
	std::cout << w1;

	DoubleWektor w2 = w1;
	std::cout << w2;

	//My code
	DoubleWektor w3(5);
	w3.set(1.11);
	std::cout << w3;

	DoubleWektor w4(3);
	w4.set(2.25);
	std::cout << w4;

	DoubleWektor w5 = w3 + w4;
	std::cout << w5;

	std::cout << w5[4] << std::endl;

	return 0;
}
